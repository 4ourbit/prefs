On Windows XP:

Set %HOME% to %USERPROFILE%\AppData

On Windows 7:

Set %HOME% to %USERPROFILE%\AppData\Roaming

Run as Admin:

New-Symlink $env:USERPROFILE\.atom -TargetPath $env:APPDATA\.atom