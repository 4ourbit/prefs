# Your init script
#
# Atom will evaluate this file each time a new window is opened. It is run
# after packages are loaded/activated and after the previous editor state
# has been restored.
#
# An example hack to make opened Markdown files always be soft wrapped:
#
# path = require 'path'
#
# atom.workspaceView.eachEditorView (editorView) ->
#   editor = editorView.getEditor()
#   if path.extname(editor.getPath()) is '.md'
#     editor.setSoftWrap(true)
#
# An example hack to utilize remote (i.e. main-process-side modules):
#
# var app = require('electron').remote.app
# app.setUserTasks [
#   {
#     program: process.execPath
#     arguments: '--new-window --dev'
#     iconPath: process.execPath
#     iconIndex: 0
#     title: 'New Window (Dev Mode)'
#     description: 'Create a new window'
#   }
# ]
#
# The old way (see http://blog.atom.io/2015/11/17/electron-api-changes.html):
#
# remote = require('remote')
#
# globalShortcut = remote.require('global-shortcut')
# globalShortcut.register 'alt+space', ->
#   console.log('alt+space is pressed')

# TODO run install command to synchronize starred packages
# apm stars --install

zoomFactor = 1.0
atom.commands.add 'atom-workspace', 'application:increase-scale', ->
  zoomFactor += 0.1
  require('web-frame').setZoomFactor(zoomFactor)
atom.commands.add 'atom-workspace', 'application:decrease-scale', ->
  zoomFactor -= 0.1
  require('web-frame').setZoomFactor(zoomFactor)

process.env.Path += ";C:\\Repositories\\ledger\\build.vc10\\Release"

# TODO show workspace state directory to know what folder to reopen next time
# https://github.com/atom/atom/pull/9968
