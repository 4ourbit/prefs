# You want to dot-source this script at start of powershell
# powershell -Command "Invoke-Expression '. (Resolve-Path $env:APPDATA\ConEmu_profile.ps1)'"

########

trap {"Error occured while loading PROFILE."}

########

# Set $PROFILE for the current user/host to this file. The default location would
# be "{ENV:USERPROFILE}\Documents\MicrosoftPowerShell\profile.ps1" otherwise.
$conemu_profile = [psobject] $MyInvocation.MyCommand.Definition
$conemu_profile | Add-Member NoteProperty CurrentUserCurrentHost $conemu_profile
$conemu_profile | Add-Member NoteProperty CurrentUserAllHosts $PROFILE.CurrentUserAllHosts
# $conemu_profile | Add-Member NoteProperty AllUsersCurrentHost $PROFILE.AllUsersCurrentHost
# $conemu_profile | Add-Member NoteProperty AllUsersAllHosts $PROFILE.AllUsersAllHosts
Set-Variable -Name PROFILE -Value $conemu_profile -Option readonly
Remove-Variable conemu_profile

Out-Host -InputObject "Loading PROFILE: $($MyInvocation.MyCommand.Definition)"

########

# TODO only load when git is loaded..
$env:Path = "$env:Path;$env:ProgramFiles\Java\jdk1.7.0_09\bin;$env:LocalAppData\git-tf"

########

$PSDefaultParameterValues['Get-Help:ShowWindow'] = $true

########

Import-Module pscx
# TODO requires Atom in PATH
$Pscx:Preferences.TextEditor = "atom.cmd"

# Import-Module password-store

########

# also requires posh-git (GitHub for Windows) and pscx to work
function Edit-GitConfig
{
  Edit-File (Join-Path $(Get-GitStatus).GitDir config)
}

########

function SuSE-Get-ChildItem
{
  Update-FormatData (Join-Path (Split-Path -Parent $PROFILE) FileSystem.format.ps1xml)
  Get-ChildItem | Format-Table -HideTableHeaders -View linux
}

function SuSE-Set-LocationEx-Parent
{
  Set-LocationEx ..
}

# -----------------------------------------------------------------------
# Cmdlet SuSE aliases
# -----------------------------------------------------------------------
Set-Alias l    SuSE-Get-ChildItem
Set-Alias cd.. SuSE-Set-LocationEx-Parent
Set-Alias ..   SuSE-Set-LocationEx-Parent

#######

Set-Alias which where.exe

#######

$MaximumHistoryCount = 31KB
$PoshHistoryPath = "$env:APPDATA\ConEmu_history.xml"

# Load history if history file exists
if (Test-path $PoshHistoryPath)
  { Import-CliXml $PoshHistoryPath | Add-History }

# Save history on exit, remove duplicates
Register-EngineEvent PowerShell.Exiting {
  Get-History -Count $MaximumHistoryCount | Group CommandLine |
  Foreach {$_.Group[0]} | Export-CliXml $PoshHistoryPath
  } -SupportEvent

function Remove-History
{
  get-history | out-gridview -title "Select entries to remove from history" -PassThru | foreach { clear-history -id $_.id}
}

######

# https://superuser.com/questions/495520/duplicate-session-in-conemu
function Create-Console($path = $(pwd)) {
  $console = [io.path]::combine("$env:PROGRAMW6432", "ConEmu", "ConEmu64.exe")
  . $console /dir "$path" /cmd '{PowerShell}' -new_console:n
}
Set-Alias sh Create-Console

######

$t = @'
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Provider;

namespace Microsoft.PowerShell.Commands {
  public class Shell {
    public string Name { get; set; }
    public object ParentFolder { get; set; }
    public object RelativePath { get; set; }
    public object AbsolutePath { get; set; }
    public object Category { get; set; }
    public object Roamable { get; set; }
    public object PreCreate { get; set; }
  }

  public class ShellContentReaderWriter : IContentReader, IContentWriter, IDisposable {
    private string path;
    private ShellProvider provider;

    internal ShellContentReaderWriter(string path, ShellProvider provider) {
      this.path = path;
      this.provider = provider;
    }

    private bool contentRead;
    public IList Read(long readCount) {
      IList list = (IList) null;
      if (!this.contentRead) {
        var item = provider.FolderDescriptions[path];
        if (item != null) {
          object valueOfItem = provider.GetValueOfItem(item);
          if (valueOfItem != null) {
            list = valueOfItem as IList;
            if (list == null) list = (IList) new object[1] {valueOfItem};
          }
          this.contentRead = true;
        }
      }
      return list;
    }

    public IList Write(IList content) { return content; }
    public void Seek(long offset, SeekOrigin origin) {}
    public void Close() {}
    public void Dispose() { Close(); }
  }

  [CmdletProvider("ShellCommands", ProviderCapabilities.ShouldProcess)]
  public class ShellProvider : ContainerCmdletProvider, IContentCmdletProvider {
    private RegistryKey RegKey { set; get; }

    protected override PSDriveInfo RemoveDrive(PSDriveInfo drive) {
      if (RegKey != null) RegKey.Close();
      return drive;
    }

    protected override Collection<PSDriveInfo> InitializeDefaultDrives() {
      PSDriveInfo drive = new PSDriveInfo("shell", ProviderInfo, string.Empty, string.Empty, null);
      return new Collection<PSDriveInfo>(new[] {drive});
    }

    private IDictionary folderDescriptions;
    public IDictionary FolderDescriptions {
      get {
        if (folderDescriptions != null) return folderDescriptions;

        var fdList = new Collection<DictionaryEntry>();
        try {
          const string hklmRegKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FolderDescriptions";
          RegKey = Registry.LocalMachine.OpenSubKey(hklmRegKey);
        }
        catch (Exception ex) {
            WriteError(new ErrorRecord(ex, "FolderDescriptionsUnavailable", ErrorCategory.ResourceUnavailable, FolderDescriptions));
        }
        foreach (string name in RegKey.GetSubKeyNames()) {
          var subKey = RegKey.OpenSubKey(name, RegistryKeyPermissionCheck.ReadSubTree);
          var subValue = new Shell {
            Name = subKey.GetValue("Name").ToString(),
            ParentFolder = subKey.GetValue("ParentFolder"),
            RelativePath = subKey.GetValue("RelativePath"),
            AbsolutePath = string.Empty,
            Category = subKey.GetValue("Category"),
            Roamable = subKey.GetValue("Roamable"),
            PreCreate = subKey.GetValue("PreCreate")
          };
          fdList.Add(new DictionaryEntry(name, subValue));
          subKey.Close();
        }
        var fdDict = fdList.ToDictionary(entry => entry.Key, entry => entry.Value);
        fdDict.Keys.ToList().ForEach((key) => {
            var value = (Shell)fdDict[key];
            value.AbsolutePath = Environment.GetEnvironmentVariable("USERPROFILE");
            if (value.ParentFolder != null)
                if (fdDict.ContainsKey(value.ParentFolder)) {
                  var parent = (Shell)fdDict[value.ParentFolder];
                  if (parent.RelativePath != null)
                    value.AbsolutePath += "\\" + parent.RelativePath.ToString();
                }
            if (value.RelativePath != null)
              value.AbsolutePath += "\\" + value.RelativePath.ToString();
        });
        folderDescriptions = fdDict.ToDictionary(entry => ((Shell)entry.Value).Name, entry => entry.Value);
        return folderDescriptions;
      }
    }

    public object GetValueOfItem(object item) {
      return ((Shell)item).AbsolutePath;
    }

    protected override bool ItemExists(string path) {
      if (string.IsNullOrEmpty(path)) return true;
      return FolderDescriptions.Contains(path);
    }

    protected override bool IsValidPath(string path) {
      return ItemExists(path);
    }

    protected override void GetItem(string path) {
      if (string.IsNullOrEmpty(path))
        WriteItemObject(FolderDescriptions.Values, path, true);
      else if (FolderDescriptions.Contains(path))
        WriteItemObject(FolderDescriptions[path], path, false);
    }

    protected override void InvokeDefaultAction(string path) {
      if (ItemExists(path)) Process.Start("explorer", "shell:" + path);
    }

    protected override void GetChildItems(string path, bool recurse) {
      if (string.IsNullOrEmpty(path)) {
        List<DictionaryEntry> list = new List<DictionaryEntry>(FolderDescriptions.Count + 1);
        foreach (DictionaryEntry dictionaryEntry in FolderDescriptions)
          list.Add(dictionaryEntry);
        list.Sort(((left, right) => StringComparer.CurrentCultureIgnoreCase.Compare((string) left.Key, (string) right.Key)));
        foreach (var dictionaryEntry in list)
          WriteItemObject(dictionaryEntry.Value, (string) dictionaryEntry.Key, false);
      } else if (FolderDescriptions.Contains(path))
          WriteItemObject(FolderDescriptions[path], path, false);
    }

    protected override void GetChildNames(string path, ReturnContainers returnContainers) {
      if (string.IsNullOrEmpty(path)) {
        foreach (DictionaryEntry dictionaryEntry in FolderDescriptions)
          WriteItemObject(dictionaryEntry.Key, (string) dictionaryEntry.Key, false);
      } else if (folderDescriptions.Contains(path))
          WriteItemObject(path, path, false);
    }

    protected override bool HasChildItems(string path) {
      if (string.IsNullOrEmpty(path))
        if (FolderDescriptions.Count > 0)
          return true;
      return false;
    }

    public IContentReader GetContentReader(string path) {
      return new ShellContentReaderWriter(path, this);
    }

    public IContentWriter GetContentWriter(string path) {
      return new ShellContentReaderWriter(path, this);
    }

    public object GetContentReaderDynamicParameters(string path) { return null; }
    public object GetContentWriterDynamicParameters(string path) { return null; }
    public void ClearContent(string path) {}
    public object ClearContentDynamicParameters(string path) { return null; }
  }
}
'@

$ProviderType = Add-Type -TypeDefinition $t -PassThru
Import-Module -Assembly $ProviderType.assembly
Remove-Variable t, ProviderType

function Get-ShellCommand { param([Parameter(Position=1)]$Command)
  Get-ChildItem shell: | Where-Object -Property Name -Match $Command |
    Sort-Object -Property Name | Format-Table Name, AbsolutePath
}

Set-Alias -Name gsc -Value Get-ShellCommand

########

# Short errors
$ErrorView = "CategoryView"

########

# Uses Pscx cd alias if available
cd $shell:Downloads | Out-Null
