(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(backup-directory-alist (quote ((".*" . "~/.emacs.d/backup"))))
 '(blink-cursor-interval 0.5)
 '(blink-cursor-mode t)
 '(column-number-mode t)
 '(custom-enabled-themes (quote (sanityinc-tomorrow-day)))
 '(custom-safe-themes t)
 '(delete-old-versions (quote keep-all-versions))
 '(delete-selection-mode t)
 '(desktop-save-mode t)
 '(fill-column 80)
 '(global-font-lock-mode t)
 '(global-hl-line-mode t)
 '(global-linum-mode nil)
 '(history-length 1000)
 '(ido-decorations (quote ("
  ▶ " "" "
    " "
    ..." "  ▶ " "" "
  ✗[No match]" "
  ✓ [Matched]" "
  ✗[Not readable]" "
  ✗[Too big]" "
  ↵ [Confirm]")))
 '(ido-everywhere t)
 '(ido-max-prospects 8)
 '(ido-max-work-file-list 50)
 '(ido-mode (quote both) nil (ido))
 '(ido-save-directory-list-file "~/.emacs.d/.ido.last")
 '(ido-unc-hosts (quote ido-unc-hosts-net-view))
 '(ido-use-virtual-buffers t)
 '(menu-bar-mode nil)
 '(mouse-wheel-mode t)
 '(org-clock-persist t)
 '(org-clock-persist-file "~/.emacs.d/.org.clock")
 '(org-confirm-babel-evaluate nil)
 '(recentf-exclude (quote ("c:/Program.Files.*")))
 '(recentf-filename-handlers (quote (abbreviate-file-name)))
 '(recentf-max-saved-items 999)
 '(recentf-mode t)
 '(recentf-save-file "~/.emacs.d/.recentf")
 '(safe-local-variable-values (quote ((rainbow-mode . t))))
 '(save-place t nil (saveplace))
 '(savehist-file "~/.emacs.d/.savehist")
 '(savehist-mode t)
 '(scroll-conservatively 1000)
 '(show-paren-mode t)
 '(text-scale-mode-step 1.1)
 '(tool-bar-mode nil)
 '(transient-mark-mode t)
 '(version-control t nil nil "Make numeric backup versions")
 '(visible-bell t)
 '(visual-line-fringe-indicators (quote (nil right-curly-arrow))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-line ((t (:inherit highlight :background "systeminfowindow")))))
