;;; 4b.el --- Personal function replacements.

;;; Code:

;; borrowed from ido.el and modified to not highlight first match
(defun 4b/ido-completions (name)
  ;; Return the string that is displayed after the user's text.
  ;; Modified from `icomplete-completions'.

  (let* ((comps ido-matches)
         (ind (and (consp (car comps)) (> (length (cdr (car comps))) 1)
                   ido-merged-indicator))
         first)

    (if (and ind ido-use-faces)
        (put-text-property 0 1 'face 'ido-indicator ind))

    ;; (if (and ido-use-faces comps)
    ;;  (let* ((fn (ido-name (car comps)))
    ;;         (ln (length fn)))
    ;;    (setq first (format "%s" fn))
    ;;    (put-text-property 0 ln 'face
    ;;                       (if (= (length comps) 1)
    ;;                              (if ido-incomplete-regexp
    ;;                                  'ido-incomplete-regexp
    ;;                                'ido-only-match)
    ;;                         'ido-first-match)
    ;;                       first)
    ;;    (if ind (setq first (concat first ind)))
    ;;    (setq comps (cons first (cdr comps)))))

    (cond ((null comps)
           (cond
            (ido-show-confirm-message
             (or (nth 10 ido-decorations) " [Confirm]"))
            (ido-directory-nonreadable
             (or (nth 8 ido-decorations) " [Not readable]"))
            (ido-directory-too-big
             (or (nth 9 ido-decorations) " [Too big]"))
            (ido-report-no-match
             (nth 6 ido-decorations))  ;; [No match]
            (t "")))
          (ido-incomplete-regexp
           (concat " " (car comps)))
          ((null (cdr comps))           ;one match
           (concat (if (if (not ido-enable-regexp)
                           (= (length (ido-name (car comps))) (length name))
                         ;; We can't rely on the length of the input
                         ;; for regexps, so explicitly check for a
                         ;; complete match
                         (string-match name (ido-name (car comps)))
                         (string-equal (match-string 0 (ido-name (car comps)))
                                       (ido-name (car comps))))
                       ""
                     ;; when there is one match, show the matching file name in full
                     (concat (nth 4 ido-decorations)  ;; [ ... ]
                             (ido-name (car comps))
                             (nth 5 ido-decorations)))
                   (if (not ido-use-faces) (nth 7 ido-decorations))))  ;; [Matched]
          (t                            ;multiple matches
           (let* ((items (if (> ido-max-prospects 0) (1+ ido-max-prospects) 999))
                  (alternatives
                   (apply
                    #'concat
                    (cdr (apply
                          #'nconc
                          (mapcar
                           (lambda (com)
                             (setq com (ido-name com))
                             (setq items (1- items))
                             (cond
                              ((< items 0) ())
                              ((= items 0) (list (nth 3 ido-decorations))) ; " | ..."
                              (t
                               (list (or ido-separator (nth 2 ido-decorations)) ; " | "
                                     (let ((str (substring com 0)))
                                       (if (and ido-use-faces
                                                (not (string= str first))
                                                (ido-final-slash str))
                                           (put-text-property 0 (length str) 'face 'ido-subdir str))
                                       str)))))
                           comps))))))

             (concat
              ;; put in common completion item -- what you get by pressing tab
              (if (and (stringp ido-common-match-string)
                       (> (length ido-common-match-string) (length name)))
                  (concat (nth 4 ido-decorations)   ;; [ ... ]
                          (substring ido-common-match-string (length name))
                          (nth 5 ido-decorations)))
              ;; list all alternatives
              (nth 0 ido-decorations)  ;; { ... }
              alternatives
              (nth 1 ido-decorations)))))))

;; (defun ido-pop-to-visible-buffer (buffer &optional action norecord)
;;   "This is a `pop-to-buffer' rip-off, except it pre-selects the
;; other visible buffer."
;;   (interactive (list (read-buffer "Pop to buffer: "
;;                                   (other-buffer (current-buffer) t))
;;                      (if current-prefix-arg t)))
;;   (setq buffer (window-normalize-buffer-to-switch-to buffer))
;;   (set-buffer buffer)
;;   (let* ((old-frame (selected-frame))
;;          (window (display-buffer buffer action))
;;          (frame (window-frame window)))
;;     ;; If we chose another frame, make sure it gets input focus.
;;     (unless (eq frame old-frame)
;;       (select-frame-set-input-focus frame norecord))
;;     ;; Make sure new window is selected (Bug#8615), (Bug#6954).
;;     (select-window window norecord)
;;     buffer))
;; (define-key my-keys-minor-mode-map (kbd "C-M-o") 'ido-pop-to-visible-buffer)

(provide '4b)
