;; Use GitHub for Windows gnuwin32-Executables
(defun github-portable-git-bin-path ()
  (setq file (concat (getenv "LOCALAPPDATA") "\\GitHub\\shell.ps1"))
  (with-temp-buffer
    (insert-file-contents file)
    (goto-char 1)
    (search-forward-regexp "\\\\Github\\\\PortableGit_\\w+")
    (setq portable-git-path (match-string-no-properties 0)))
  (concat (getenv "LOCALAPPDATA") portable-git-path "\\bin"))

;; Adjust Emacs utility search path
(setq exec-path
      (append
       (list (replace-regexp-in-string "\\\\" "\/"
                                       (github-portable-git-bin-path))) exec-path))
;; Adjust In-Emacs shell environment
(setenv "PATH"
  (concat
   (getenv "PATH") ";"
   (github-portable-git-bin-path)))

;; Use GitHub for Windows installation git-Executable
(require 'magit)
(setq magit-git-executable (concat (github-portable-git-bin-path) "\\git.exe"))