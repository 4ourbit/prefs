;; (add-to-list 'load-path "~/.emacs.d/")
;; (require '4b)
;; (require 'keys)

(setq custom-file     "~/.emacs.d/custom.el")
(defvar 4b-file       "~/.emacs.d/4b.el")
(defvar my-keys-mode  "~/.emacs.d/keys.el")
(defvar user-file     "~/.emacs.d/user.el")

;; "y" and "n" instead of "yes" and "no"
(defalias 'yes-or-no-p 'y-or-n-p)

;; Set frame title bar
(setq frame-title-format
      '(" " (:eval (if (buffer-file-name) (abbreviate-file-name (buffer-file-name)) "%b"))))

;;; Key remappings
(define-key input-decode-map (kbd "M-?") (kbd "C-h"))

(define-key input-decode-map (kbd "C-k") (kbd "<up>"))
(define-key input-decode-map (kbd "C-j") (kbd "<down>"))
(define-key input-decode-map (kbd "C-l") (kbd "C-<left>"))
(define-key input-decode-map (kbd "C-�") (kbd "C-<right>"))

(define-key input-decode-map (kbd "C-M-j") (kbd "C-<down>"))
(define-key input-decode-map (kbd "C-M-k") (kbd "C-<up>"))
(define-key input-decode-map (kbd "C-M-�") (kbd "<end>"))
(define-key input-decode-map (kbd "C-M-l") (kbd "<home>"))

(define-key input-decode-map (kbd "C-h") (kbd "<backspace>"))
(define-key input-decode-map (kbd "C-M-h") (kbd "C-<backspace>"))

;; (define-key my-keys-minor-mode-map (kbd "C-M-h") 'backward-kill-word)
;; (define-key my-keys-minor-mode-map (kbd "M-�") 'backward-kill-word)
;;(define-key input-decode-map (kbd "C-m") (kbd "<return>"))

;;; Global key bindings

;; "C-x C-SPC" is the default for `pop-global-mark'
;; pop-to-mark-command is also accessible via C-u C-SPC
(global-set-key (kbd "C-c C-SPC") 'pop-to-mark-command)
(global-set-key (kbd "<mouse-3>") 'mouse-buffer-menu)

;;; Define and load my-keys-minor-mode
(load my-keys-mode)

;;; Mode-specific key bindings

(defun ido-my-keys ()
  "Add my keybindings for ido."
  (define-key ido-completion-map [remap backward-paragraph] 'ido-prev-match) ; "C-M-j"
  (define-key ido-completion-map [remap forward-paragraph] 'ido-next-match)  ; "C-M-m"
  (define-key ido-completion-map [remap forward-paragraph] 'ido-next-match)  ; "C-M-m"
  (define-key ido-completion-map [remap forward-paragraph] 'ido-next-match)  ; "C-M-m"
  (define-key ido-buffer-completion-map (kbd "C-d") 'ido-kill-buffer-at-head)

  ;; ido-first-match highlighting overwrites virtual buffers highlighting
  (load 4b-file)
  (fset 'ido-completions '4b/ido-completions))
(add-hook 'ido-setup-hook 'ido-my-keys)

;;; Basic Emacs styling

(set-default 'cursor-type '(bar . 2))
(toggle-indicate-empty-lines)

;; Add packages from milkbox.net
(require 'package)
(add-to-list 'package-archives
       '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)

;; Keep installed packages up to date
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar prelude-packages
  '(browse-kill-ring
    color-theme-sanityinc-tomorrow
    expand-region
    elisp-slime-nav
    ethan-wspace
    flx-ido        ; fuzzier ido
    ido-ubiquitous ; idofy more, substitutes ido-hacks
    ido-at-point   ; idofy completion-at-point
    ; ledger-mode
    flycheck-ledger; also installs flycheck, an alternative to flymake
    keyfreq
    magit
    markdown-mode
    popwin         ; pop up window for help, browse-kill-ring ...
    volatile-highlights
  )
  "A list of packages to ensure are installed at launch.")

(mapc (lambda (package)
  (or (package-installed-p package)
      (if (yes-or-no-p (format "Package %s is missing. Install it? " package))
  (package-install package))))
      prelude-packages)

;;; Concerning Emacs built-in modes

;; Load custom settings, customizations go in here
(load custom-file)

(server-start)

;; For pairing parenthesis
;; (require 'autopair)

;; To save the clock history across Emacs sessions
(org-clock-persistence-insinuate)

;; ask the user if they wish to clock out before killing Emacs
(defun 4b/org-query-clock-out ()
  "Ask the user before clocking out."
  (if (and (featurep 'org-clock)
           (funcall 'org-clocking-p)
           (yes-or-no-p "You are currently clocking time, clock out? "))
      (org-clock-out)
    t)) ;; only fails on keyboard quit or error
(add-hook 'kill-emacs-query-functions '4b/org-query-clock-out)

;; For spell checking in org-mode
;; (add-hook 'org-mode-hook 'turn-on-flyspell)

(setq org-latex-create-formula-image-program 'dvipng)
(setq org-latex-preview-ltxpng-directory "~/.emacs.d/ltxpng/")

;;; Concerning Emacs third-party modes

(when (require 'expand-region nil 'noerror)

  ;; Overwrites `mark-sexp'
  (define-key my-keys-minor-mode-map (kbd "C-M-SPC") 'er/expand-region)
  (define-key my-keys-minor-mode-map (kbd "C-M-S-SPC") 'er/contract-region)

  ;; Overwrites `mark-paragraph'
  (define-key my-keys-minor-mode-map (kbd "M-j") 'er/expand-region)
  (define-key my-keys-minor-mode-map (kbd "M-J") 'er/contract-region))


(when (require 'keyfreq nil 'noerror)
  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1))

(when (require 'browse-kill-ring nil 'noerror)
  (browse-kill-ring-default-keybindings))

(when (require 'magit nil 'noerror)
  (add-hook 'magit-mode-hook
            (lambda ()
              (define-key magit-mode-map (kbd "C-j") 'magit-goto-next-section)
              (define-key magit-mode-map (kbd "C-h") 'magit-goto-previous-section))))

(when (require 'popwin)
  ;; close help with SPC
  (define-key popwin:keymap " " 'popwin:close-popup-window)

  (push '(Custom-mode) popwin:special-display-config)
  (push '(Info-mode) popwin:special-display-config)

  ;; Show Kill Ring in Popup window
  ;; (add-hook 'popwin:after-popup-hook
  ;;         (lambda (popwin:update-window-reference
  ;;                  'browse-kill-ring-original-window :safe t)))
  (push "*Kill Ring*" popwin:special-display-config)

  ;; Use Popup windows for magit
  (push '(git-commit-mode :stick t) popwin:special-display-config)
  (push '("*magit-commit*" :stick t) popwin:special-display-config)
  (push "*magit-diff*" popwin:special-display-config)
  (push "*magit-process*" popwin:special-display-config)
  (push "*magit-edit-log*" popwin:special-display-config)

  (push '("*Completions*" :noselect t) popwin:special-display-config)
  (push '("*Messages*" :noselect t :height 30) popwin:special-display-config)
  (push '("*Apropos*" :noselect t :height 30) popwin:special-display-config)

  (popwin-mode t))

;; Set up ethan-wspace-mode to merge highlighting with theme color
;; Deprecates require-final-newlines and show-trailing-whitespace
(when (require 'ethan-wspace nil 'noerror)
  (global-ethan-wspace-mode t))

;; Set up elisp-slime-nav
(dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook))
  (add-hook hook 'turn-on-elisp-slime-nav-mode))

;; Set up volatile-highlights-mode
(when (require 'volatile-highlights)
  (setq vhl/default-face '(t (:inherit custom-comment)))
  (volatile-highlights-mode t))

;; Set up markdown-mode
(autoload 'markdown-mode "markdown-mode.el"
  "Major mode for editing Markdown files" t)
(setq auto-mode-alist (cons '("\\.md" . markdown-mode) auto-mode-alist))

;; Set up ido related tools
(when (require 'ido-ubiquitous)
  (ido-ubiquitous-mode t)
  (setq ido-ubiquitous-command-overrides
  '(;; Disable, when installing smex
    (enable-old exact "execute-extended-command")
    ;; https://github.com/technomancy/ido-ubiquitous/issues/13#issuecomment-8033522
    (enable prefix "wl-")
    ;; https://github.com/technomancy/ido-ubiquitous/issues/7
    (enable-old prefix "Info-")
    ;; https://github.com/DarwinAwardWinner/ido-ubiquitous/issues/4
    (enable exact "webjump")
    ;; https://github.com/DarwinAwardWinner/ido-ubiquitous/issues/28
    (enable regexp "\\`\\(find\\|load\\|locate\\)-library\\'")
    ;; https://github.com/DarwinAwardWinner/ido-ubiquitous/issues/37
    ;; Org and Magit already support ido natively
    (disable prefix "org-")
    (disable prefix "magit-"))))

(when (require 'flx-ido)
  ;; flx highlighting overwrites virtual buffers highlighting
  (setq ido-enable-flex-matching t)
  (setq flx-ido-use-faces 'nil)
  (flx-ido-mode t))

(when (require 'ido-at-point)
  ;; fuzzy ido-at-point clashes with ledger pcomplete
  ;; but due to flx-ido-mode its already fuzzy
  (setq ido-at-point-fuzzy nil)
  (ido-at-point-mode t))

;; Set up ledger-mode
(autoload 'ledger-mode "ledger-mode.el"
  "Major mode for editing ledger files" t)
(setq ledger-highlight-xact-under-point nil)

;; allow to babel ledger in org mode
(require 'ob-ledger)


;; Configure flycheck-mode
(when (require 'flycheck)
  (eval-after-load 'flycheck
    '(require 'flycheck-ledger))
  (setq flycheck-disabled-checkers '(emacs-lisp-checkdoc))
  (setq global-flycheck-mode t))

;; Install and set up emacs-goodies
;;(add-to-list 'load-path "~/.emacs.d/emacs-goodies-el/")
;;(require 'emacs-goodies-el)

;;(toggle-save-place-globally)

;; (package-initialize) is called automatically when
;; package-enable-at-startup is not nil

;;; Load user settings
(load user-file)
