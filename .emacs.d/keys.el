;;; My-Keys key bindings

(defvar my-keys-minor-mode-map (make-keymap) "keys-mode keymap.")
(define-minor-mode my-keys-minor-mode
  "A minor mode so that my key settings override any major modes." t " Keys"
  'my-keys-minor-mode-map)
(defadvice load (after give-my-keybindings-priority)
  "Try to ensure that my keybindings always have priority."
  (if (not (eq (car (car minor-mode-map-alist)) 'my-keys-minor-mode))
      (let ((mykeys (assq 'my-keys-minor-mode minor-mode-map-alist)))
        (assq-delete-all 'my-keys-minor-mode minor-mode-map-alist)
        (add-to-list 'minor-mode-map-alist mykeys))))
(ad-activate 'load)

;; from SuperCollider
(define-key my-keys-minor-mode-map (kbd "C-.") 'keyboard-escape-quit)

(define-key my-keys-minor-mode-map (kbd "<C-tab>") 'switch-to-prev-buffer)
(define-key my-keys-minor-mode-map (kbd "<C-S-tab>") 'switch-to-next-buffer)
(define-key my-keys-minor-mode-map (kbd "M-k") 'kill-this-buffer)

(define-key my-keys-minor-mode-map (kbd "C-M-d") 'kill-line)

(define-key my-keys-minor-mode-map (kbd "C-z") 'undo)

;; goes same place as "M-/" with german keyboard layout
(define-key my-keys-minor-mode-map (kbd "M--") 'completion-at-point)

(define-globalized-minor-mode
  global-text-scale-mode
  text-scale-mode
  (lambda () (text-scale-mode 1)))

(defun global-text-scale-adjust (inc)
  "Adjust text-scale in all windows in all frames."
  (interactive)
  (text-scale-set 1)
  (kill-local-variable 'text-scale-mode-amount)
  (setq-default text-scale-mode-amount (+ text-scale-mode-amount inc))
  (global-text-scale-mode 1))

;; Scale text in all windows in all frames
(define-key my-keys-minor-mode-map (kbd "C-:")
  '(lambda () (interactive) (global-text-scale-adjust 1)))
(define-key my-keys-minor-mode-map (kbd "C-;")
  '(lambda () (interactive) (global-text-scale-adjust -1)))

;;move the active region (with respect to columns)
;;or the current line prefix arg lines up or down.
(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (beginning-of-line)
    (when (or (> arg 0) (not (bobp)))
      (forward-line)
      (when (or (< arg 0) (not (eobp)))
        (transpose-lines arg))
      (forward-line -1)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))

;; (define-key my-keys-minor-mode-map (kbd "M-j") 'move-text-up)
;; (define-key my-keys-minor-mode-map (kbd "M-m") 'move-text-down)

;; Overwrites `split-line'
(define-key my-keys-minor-mode-map (kbd "C-M-o") 'ido-switch-buffer-other-window)

;; Overwrites `open-line'
(define-key my-keys-minor-mode-map (kbd "C-o") 'other-window)

(my-keys-minor-mode t)
